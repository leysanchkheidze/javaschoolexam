package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CalculatorDijkstra {
    final static String ROUNDING_NEEDED_REGEX = "-?\\d*\\.\\d*";
    final static Pattern ROUNDING_NEEDED_PATTERN = Pattern.compile(ROUNDING_NEEDED_REGEX);
    final static String DECIMAL_ZERO_REGEX = "-?\\d*\\.0*";
    final static Pattern DECIMAL_ZERO_PATTERN = Pattern.compile(DECIMAL_ZERO_REGEX);
    final static String ALLOWED_SYMBOLS_REGEX = "[\\d\\s().+\\-/*]+";
    final static Pattern ALLOWED_SYMBOLS_PATTERN = Pattern.compile(ALLOWED_SYMBOLS_REGEX);
    final static String VALID_END_REGEX = ".*[)\\d]";
    final static Pattern VALID_END_PATTERN = Pattern.compile(VALID_END_REGEX);
    final static int DECIMAL_DIGITS = 4;
    final static Set<Character> UNREPEATABLE_SYMBOLS = new HashSet<>(Arrays.asList('+', '-', '/', '*', '.'));

    private final Stack<Double> numbers = new Stack<>();
    private final Stack<Character> operators = new Stack<>();

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (null == statement) {
            return null;
        }
        statement = statement.trim();

        //validate if input isn't empty, contains only valid symbols,
        //all braces are closed, arithmetical signs aren't duplicated and aren't hanging at the end:
        if (!validateNotEmpty(statement) ||
                !validateStatementSymbols(statement) ||
                !validateClosedBraces(statement) ||
                !validateNoRepeated(statement) ||
                !validateStatementEnd(statement)) {
            return null;
        }

        String result = processString(statement);
        if (result.contains("Infinity") || "NaN".equals(result)) {
            return null;
        }

        if (isRoundingNeeded(result)) {
            return round(result);
        }
        return result;
    }

    private String processString(String statement) {
        for (int i = 0; i < statement.length(); ) {
            if (Character.isDigit(statement.charAt(i))) {
                int start = i;
                //collect following digits and . and push whole number to numbers stack:
                while (i < statement.length() && (Character.isDigit(statement.charAt(i))
                        || statement.charAt(i) == '.')) {
                    i++;
                }
                numbers.push(Double.parseDouble(statement.substring(start, i)));
                continue;
            }

            if (isMathOperator(statement.charAt(i))) {
                //opening new sequence in braces:
                if (statement.charAt(i) == '(') {
                    operators.push(statement.charAt(i));

                //closing sequence in braces, all collected should be calculated until opening brace:
                } else if (statement.charAt(i) == ')') {
                    while (!operators.isEmpty() && operators.peek() != '(') {
                        calculateToStack();
                    }
                    //remove opening brace to finish sequence:
                    operators.pop();
                } else {
                    // if stack contains operators higher or equal to new one, calculate them.
                    // then store new operator to stack
                    while (!operators.isEmpty() && compareOperators(statement.charAt(i), operators.peek()) <= 0) {
                        calculateToStack();
                    }
                    operators.push(statement.charAt(i));
                }
            }
            i++;
        }

        //calculate all remaining in stack operations:
        while (operators.size() > 0) {
            calculateToStack();
        }

        return numbers.pop().toString();
    }

    private void calculateToStack() {
        double second = numbers.pop();
        double first = numbers.pop();
        double intermediateResult = calculate(first, second, operators.pop());
        numbers.push(intermediateResult);
    }

    private int getOperatorRank(char operator) {
        // * and / are higher than + and -
        if (operator == '*' || operator == '/') {
            return 1;
        } else {
            return 0;
        }
    }

    private int compareOperators(char firstOperator, char secondOperator) {
        //opening brace is always first rank:
        if (secondOperator == '(') {
            return 1;
        }
        int firstRank = getOperatorRank(firstOperator);
        int secondRank = getOperatorRank(secondOperator);
        return firstRank - secondRank;
    }

    private double calculate(double first, double second, char operator) {
        if ('+' == operator) {
            return first + second;
        } else if ('-' == operator) {
            return first - second;
        } else if ('*' == operator) {
            return first * second;
        } else if ('/' == operator) {
            return first / second;
        }
        throw new IllegalArgumentException("Received invalid operation: " + operator);
    }

    private boolean isMathOperator(char symbol) {
        return "+-*/()".indexOf(symbol) >= 0;
    }


    private boolean validateStatementSymbols(String statement) {
        Matcher matcherSymbols = ALLOWED_SYMBOLS_PATTERN.matcher(statement);
        return matcherSymbols.matches();
    }

    private boolean validateStatementEnd(String statement) {
        Matcher matcherStatementEnd = VALID_END_PATTERN.matcher(statement);
        return matcherStatementEnd.matches();
    }

    private boolean validateClosedBraces(String statement) {
        int numOfNotClosed = 0;
        for (int i = 0; i < statement.length(); i++) {
            if (statement.charAt(i) == '(') {
                numOfNotClosed++;
            } else if (statement.charAt(i) == ')') {
                numOfNotClosed--;
            }
        }
        return numOfNotClosed == 0;
    }

    private boolean validateNotEmpty(String statement) {
        return statement.length() != 0;
    }

    private boolean validateNoRepeated(String statement) {
        for (int i = 0; i < statement.length() - 1; i++) {
            if (UNREPEATABLE_SYMBOLS.contains(statement.charAt(i))) {
                if (statement.charAt(i) == statement.charAt(i + 1)) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean isRoundingNeeded(String numberString) {
        Matcher matcherRounding = ROUNDING_NEEDED_PATTERN.matcher(numberString);
        return matcherRounding.matches();
    }

    private String round(String numberString) {
        BigDecimal number = BigDecimal.valueOf(Double.parseDouble(numberString));
        number = number.setScale(DECIMAL_DIGITS, RoundingMode.HALF_UP);

        double rounded = number.doubleValue();

        String result = String.valueOf(rounded);

        //Check if rounded number has meaningless decimal part .0 and remove it:
        Matcher matcherDecimalZero = DECIMAL_ZERO_PATTERN.matcher(result);
        if (matcherDecimalZero.matches()) {
            result = removeHangingZero(result);
        }
        return result;
    }

    private String removeHangingZero(String numberString) {
        return numberString.substring(0, numberString.indexOf("."));
    }

}
